program blabla;
uses modulos,crt;

type cc=record
	p:integer;
	nom:string[36];
  end; 
type rega=record
	num:integer;
	nome:string[36];
	cpf:string[11];
	data:string[8];	
	cargo:integer;
	n1,n2,n3,n4,n5,som,c_g,c_cl,c_o:integer;
	end;
     
var reg2:rega;
    arq2:file of rega;
    count,t,a,PAG :integer;  
		vet:array[1..9999] of cc;
		sair:boolean;
		totalPag:real;

PROCEDURE CABECA;
BEGIN
 CLRSCR ;
 PAG:=PAG+1;
 WRITELN('                         RELATORIO GERAL - ORDEM INSCRICAO             PAGINA:',PAG:3);
 WRITELN;
 write('  ORD  NUM N O M E                              ===C P F=== NASCIMENTO CS  N1');
 WRITELN('  N2  N3  N4  N5  SO  CG CC CV');
END;		

procedure mostrarvetor;
begin
CLRSCR;
	CABECA;
  totalPag:=count div 50;
  	if (frac(count/50) > 0) then
  		totalpag:=totalPag+1;
	for a := 1 to count do
  begin
  	seek (arq2, vet[a].p-1);
    read (arq2, reg2);
    writeln(a:5,reg2.num:5,' ',REG2.NOME,' ',reg2.cpf,' ',copy(reg2.data,7,2),'/',copy(reg2
    .data,5,2),'/',copy(reg2.data,1,4),reg2.cargo:3,reg2.n1:4,reg2.n2:4,REG2.N3:4
    ,REG2.N4:4,REG2.N5:4,reg2.som:4,reg2.c_g:4,reg2.c_cl:3,reg2.c_o:3 );
    if(a MOD 50 = 0) THEN
    	BEGIN
      	writeln('');
        writeln('Pagina numero: ', PAG, ' de um total de ', totalPag:2:0, '. ENTER PARA AVAN�AR.');
        readln;
        CABECA;
      END;
  end;
close(arq2);  
    PAG:=0;
    writeln('');
    writeln('TOTAL DE REGISTROS LIDOS = ',count:5);
    writeln('Pressione qualquer tecla para continuar.');
    readln;
  end;


procedure ordenavetor(tipord:integer);
Begin
	clrscr;
	count:=0;
	t:=0;
	assign(arq2,'CADAST.dir');
  reset(arq2);
	while not(eof(arq2)) do
	begin
  	read (arq2,reg2);
  		if(reg2.num<>0)then
  		begin
  		count:=count+1;
  		if (tipord=1) then vet[count].nom:=reg2.nome else
  		if (tipord=2) then vet[count].nom:=reg2.cpf else
  		if (tipord=3) then vet[count].nom:=reg2.data;
  		if (tipord=4) then 
				begin
					str(reg2.cargo, vet[count].nom);
    			insert(reg2.nome, vet[count].nom, 2);
    		end;
  		vet[count].p:=reg2.num;
  		end;
	end;
	ordem(vet,count); 
	clrscr;
	mostrarvetor;
end;

procedure menu;
var opcao:integer;
begin;
clrscr;
 	writeln('-----------MENU------------');
	writeln('-[1]---ORDEM ALFABETICA----');
	writeln('-[2]---ORDENAR POR CPF-----');
	writeln('-[3]ORDENAR POR NASCIMENTO-');
	writeln('-[4]---ORDENAR POR CARGO---');
	writeln('-[5]--------SAIR-----------');
	readln(opcao);
  case (opcao) of
		1:ordenaVetor(1);
    2:ordenaVetor(2);
    3:ordenaVetor(3);
    4:ordenaVetor(4);
    5:sair:=true;
    else
      	CLRSCR; writeln('Digite uma op��o v�lida.'); menu; 
	end; 
end;

Begin
	sair:=false;
	while (sair <> true) do
  	begin;
    	assign (arq2,'CADAST.dir');
      reset (arq2);
      count:=0; 
			menu; 
    end;  
End.