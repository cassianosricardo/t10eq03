Program eq10t05;
uses modulos,crt;

type rega=record
	num:integer;
	nome:string[36];
	cpf:string[11];
	data:string[8];	
	cargo:integer;
	n1,n2,n3,n4,n5,som,c_g,c_cl,c_o:integer;
	end;
	
	var 
    aux,erro,acertos,erros,i: integer;
		prova1,prova2: text;
		registro:rega;
		cadastro: file of rega;
    line: string[54];
    gabarito: string[50];  
    gabarito2: string[50];
    n01,n02,n03,n04,n05,soma: integer;

Begin
i:=1;
	
	assign(cadastro,'CADAST.DIR');
  assign(prova1,'prova1.TXT'); 
  assign(prova2,'prova2.TXT');
  reset(prova1);
	reset(prova2);
 	reset(cadastro);

  
  gabarito:='AAAAAAAAAADCCDBBBDAAAAAAACCCCCAAAAADDDDDBAAEDCBBEA'; //gabaritos pr� carregados
  gabarito2:='BBBBBBBBBBAAAEBCEACEBBBBBEEEEEBBBBBAAAAAADCAAEEBAD';
  
   while (not EOF(prova1)) do
  	begin
    	readln(prova1,line);
    	val(copy(line, 1, 4), aux, erro);    // copia o numero de inscri��o da prova para a vari�vel aux
    	while (i<51) do
    		begin  
    			if (i>0) and (i<21) then
    				begin
    					if (copy(line,4+i,1)=(gabarito[i])) then   //compara a resposta do candidato com o gabarito
    						begin    
    							n01:=(n01+1);        //incrementa o contador de acertos em portugu�s
    						end
    					else
    						begin
    							erros:= erros+1;
    						end;
    				end;	
    			if (i>20) and (i<41) then
    				begin
    					if (copy(line,4+i,1)=(gabarito[i])) then
    						begin    
    							n02:=(n02+1);   //incrementa o contador de acertos em matem�tica 
    						end
    					else
    						begin
    							writeln('ERROU!!');
    							erros:= erros+1;
    						end;
    				end;
					if(i>40) and (i<51) then
    				begin
    					if (copy(line,4+i,1)=(gabarito[i])) then
    						begin    
    							n03:=(n03+1);    //incrementa o contador de acertos em atualides
    						end
    					else
    						begin
    							erros:= erros+1;
    						end;
    				end;				
    	i:=i+1
			end;	
			acertos:=(n01+n02+n03);
			n01:=n01*5;
			n02:=n02*5;
			n03:=n03*10;
			soma:=(n01+n02+n03);
			
			if (aux<>0) then
			begin
				seek(cadastro,aux-1);
				read(cadastro,registro);
				registro.n1:=n01;              //atualiza os registros ap�s a corre��o da prova 1
				registro.n2:=n02;
				registro.n3:=n03;
				registro.som:=soma;            // atualiza o registro.som ap�s a corre��o da prova 1
				seek (cadastro,aux-1);
				write(cadastro,registro);	
				writeln('Num: ',aux,' ',registro.nome,' ','Acertos Totais: ',acertos,' ','Erros Totais: ',erros,' ','Portugu�s: ',n01,' ','Matem�tica: ',n02,' ','Atualidades: ',n03,' ','m�dia das disciplinas: ',((n01+n02+n03/3)):3);
				end;
				i:=1;        // zera os contadores para a corre��o da prova do pr�ximo candidato 
				n01:=0;
				n02:=0;
				n03:=0;
				acertos:=0;
				erros:=0;
				soma:=0;
		  end;
		  readln;       // faz uma pausa a espera de um enter antes da corre��o da segunda prova
		
		Writeln('Corrigindo a segunda prova...');
		
		while (not EOF(prova2)) do
  	begin
    	readln(prova2,line);
    	val(copy(line, 1, 4), aux, erro);
    	while (i<51) do
    		begin  
    			if (i>0) and (i<26) then
    				begin
    					if (copy(line,4+i,1)=(gabarito2[i])) then
    						begin    
    							n04:=(n04+1);       //incrementa o contador de acertos em conhecimento do cargo 
    						end
    					else
    						begin
    							erros:= erros+1;
    						end;
    				end;	
    			if (i>25) and (i<51) then
    				begin
    					if (copy(line,4+i,1)=(gabarito2[i])) then
    						begin    
    							n05:=(n05+1);    //incrementa o contador de acertos em inform�tica
    						end 
    					else
    						begin
    							erros:= erros+1;
    						end;
    				end;
    				i:=i+1;
		    end;
		  acertos:=(n04+n05);
			n04:=n04*4;
			n05:=n05*4;
			soma:=(n04+n05);
			
			if (aux<>0) then
			begin
				seek(cadastro,aux-1);
				read(cadastro,registro);
				registro.n4:=n04;                 //atualiza os registros ap�s a corre��o da prova 2
				registro.n5:=n05;
				registro.som:=(registro.som+soma);   //atualiza o registro.som ap�s a corre��o da prova 2
				seek (cadastro,registro.num-1);
				write(cadastro,registro);	
				writeln('Num: ',aux,' ',registro.nome,' ','Acertos Totais: ',acertos,' ','Erros Totais: ',erros,' ','Conhecimento do cargo ',n04,' ','Inform�tica: ',n05,' ','m�dia das disciplinas: ',((n04+n05/3)):3);
				end;
				i:=1;
				n04:=0;
				n05:=0;
				acertos:=0;
				erros:=0;
		  
			
		end;    
		
    
    close(cadastro);
    close(prova1);
    close(prova2);
    
    
    
End.