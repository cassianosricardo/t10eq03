program t07;
uses modulos,crt;
type rega=record
  num:integer;
  nome:string[36];
  cpf:string[11];
  data:string[8];
  cargo:integer;
  n1,n2,n3,n4,n5,som,c_g,c_cl,c_o:integer;
end;

var reg2:rega;
arq2:file of rega;
count,t,a,PAG :integer;
vet:vetor;
sair:boolean;
totalPag:real;
aux,aux2:string;

PROCEDURE CABECA(opt:integer);
BEGIN
  CLRSCR ;
  PAG:=PAG+1;
  case (opt) of
    1:aux:=('ALFAB�TICA POR CLASSIFICADOS');
    2:aux:=('CLASSIFICADOS POR CARGO');
    3:aux:=('CLASSIFICA��O GERAL');
  end;
  WRITELN('                         RELATORIO GERAL - ORDEM ',aux:26,'             PAGINA:',PAG:3);
  WRITELN;
  write('  ORD':2,'NUM':5,'N O M E':8);
  WRITELN('CAR':33,'SOM':5,' CG':4,'CC':4,'CV':4);
END;

procedure mostrarvetor(mostra:integer);
var auxc,auxc2:integer;
begin
  auxc:=1;
  auxc2:=1;
  CLRSCR;
  CABECA(mostra);
  
  for a := 1 to count do
  begin
    auxc2:=auxc;
    seek (arq2, vet[a].p-1);
    read (arq2, reg2);
    auxc:=reg2.cargo;
    if (mostra=1) then
    begin
      if	(reg2.c_o>0) then
      begin
        if auxc2=auxc then
        begin
          writeln(a:5,reg2.num:5,' ',REG2.NOME,' ',reg2.cargo:2,reg2.som:6,reg2.c_g:4,' ',reg2.c_cl:3,reg2.c_o:3 );
        end;
        if auxc2<>auxc then
        begin
          a:=a-1;
          readln;
          clrscr;
          CABECA(mostra);
        end;
      end;
    end;
    
    if (mostra=2) and (reg2.num <> 0) then
    begin
      if auxc2=auxc then
      begin
        writeln(a:5,reg2.num:5,' ',REG2.NOME,' ',reg2.cargo:2,reg2.som:6,reg2.c_g:4,' ',reg2.c_cl:3,reg2.c_o:3 );
        if (a MOD 25 = 0) THEN
        begin
          writeln('');
          //writeln('P�gina numero: ', PAG,' ENTER PARA AVAN�AR.');
          readln;
          CABECA(mostra);
        end;
      end
      else
      begin
        a:=a-1;
        readln;
        clrscr;
        CABECA(mostra);
      end;
    end;
    
    if (mostra=3) and (reg2.num <> 0) then
    begin
      writeln(a:5,reg2.num:5,' ',REG2.NOME,' ',reg2.cargo:2,reg2.som:6,reg2.c_g:4,' ',reg2.c_cl:3,reg2.c_o:3 );
      if (a MOD 50 = 0) THEN
      begin
        writeln('');
        writeln('P�gina numero: ', PAG,' ENTER PARA AVAN�AR.');
        readln;
        CABECA(mostra);
      end;
    end;
  end;
  close(arq2);
  PAG:=0;
  writeln('');
  writeln('TOTAL DE REGISTROS LIDOS = ',count:5);
  writeln('Pressione qualquer tecla para continuar.');
  readln;
end;


procedure ordenavetor(tipord:integer);
Begin
  clrscr;
  count:=0;
  t:=0;
  assign(arq2,'CADAST.dir');
  reset(arq2);
  while not(eof(arq2)) do
  begin
    read (arq2,reg2);
    
    if ((tipord=1) and (reg2.c_o>0)) then
    begin
      count:=count+1;
      
      str(reg2.c_o,vet[count].nom);
      insert(reg2.nome,vet[count].nom,2);
      vet[count].p:=reg2.num;
    end;
    
    if ((tipord=2) and (reg2.num <> 0) and (reg2.c_g<>0) or (tipord=3) and (reg2.num <> 0) and (reg2.c_g<>0)) then
    begin
      count:=count+1;
      if (tipord=3) then
      begin
        str(reg2.c_g:3,aux);
        vet[count].nom:=aux;
        vet[count].p:=reg2.num;
      end;
      
      if (tipord=2) then
      begin
        str(reg2.cargo:1,aux);
        str(reg2.c_cl:3,aux2);
        vet[count].nom:=aux+aux2  ;
        vet[count].p:=reg2.num;
      end;
    end;
  end;
  ordem(vet,count);
  clrscr;
  mostrarvetor(tipord);
end;

procedure menu;
var opcao:integer;
begin;
  clrscr;
  writeln('-----------MENU---------------------');
  writeln('-[1]---ALFABETICA(Classificados)----');
  writeln('-[2]---CARGOS(classificados)--------');
  writeln('-[3]---CLASSIFICA��O GERAL----------');
  writeln('-[4]---SAIR-------------------------');
  readln(opcao);
  case (opcao) of
    1:ordenaVetor(1);
    2:ordenaVetor(2);
    3:ordenaVetor(3);
    4:sair:=true;
    else
    CLRSCR; writeln('Digite uma op��o v�lida.'); menu;
  end;
end;

Begin
  sair:=false;
  while (sair <> true) do
  begin;
    assign (arq2,'CADAST.dir');
    reset (arq2);
    count:=0;
    menu;
  end;
End.