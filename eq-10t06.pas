Program eq10t06;

uses modulos;

type reg=record
  num:integer;
  nome:string[36];
  cpf:string[11];
  data:string[8];
  cargo:integer;
  n1,n2,n3,n4,n5,so,clg,clc,ccl:integer;
end;

var regaux:reg;
	lin:string[32];
	auxstr:string[8];
	vaga:text;
	arq: file of reg;
	vet:vetor;
	a,tot,c,d,e,pag:integer;
	vagas,cargo:array[0..6]of integer;
	auxcc:cc;

Begin
  assign(arq,'cadast.dir');
  assign(vaga,'vagas.txt');
  reset(arq);
  reset(vaga);
  
  while(not eof(arq)) do
   begin
    read(arq,regaux);
    if(regaux.num<>0) then
     begin
      tot:=tot+1;
      val(regaux.data,d,e);
        {criterios para a classificação}
      auxcc.p:=regaux.num;
      str(30000000-d:8,auxstr);
      insert(auxstr,auxcc.nom,1);
      str(regaux.n3:3,auxstr);
      insert(auxstr,auxcc.nom,1);
      str(regaux.n1:3,auxstr);
      insert(auxstr,auxcc.nom,1);
      str(regaux.n2:3,auxstr);
      insert(auxstr,auxcc.nom,1);
      str(regaux.n5,auxstr);
      insert(auxstr,auxcc.nom,1);
      str(regaux.n4:3,auxstr);
      insert(auxstr,auxcc.nom,1);
      str(regaux.so:3,auxstr);
      insert(auxstr,auxcc.nom,1);
      
      vet[tot]:=auxcc;
     end;
    end;
  
  //preenche vetor das vagas
  while(not eof(vaga)) do
   begin
    a:=a+1;
    readln(vaga,lin);
    val(copy(lin,30,2),d,e);
    vagas[a]:=d;
  end;
  
  ordem(vet,tot);
  cabeca1('NOTA',pag);
  
  a:=tot;
  d:=0;
  
	while(a>0) do
   begin
    
    d:=d+1;
    seek(arq,vet[a].p-1);
    read(arq,regaux);
    
    c:=regaux.cargo;
    regaux.clg:=d;
    
    cargo[c]:=cargo[c]+1;
    regaux.clc:=cargo[c];
    
    if(vagas[c]>0) then
     begin
      regaux.ccl:=c;
      vagas[c]:=vagas[c]-1;
     end;
    
    writeln(d:5,regaux.num:5,' ',regaux.NOME,' ',regaux.cpf,' ',copy(regaux.data,7,2),'/',copy(regaux
    .data,5,2),'/',copy(regaux.data,1,4),regaux.cargo:3,regaux.n1:4,regaux.n2:4,regaux.N3:4
    ,regaux.N4:4,regaux.N5:4,regaux.so:4,regaux.clg:4,regaux.clc:3,regaux.ccl:3 );
    
    seek(arq,vet[a].p-1);
    write(arq,regaux);
    
    if((d) mod 50 = 0) then
     begin
      readln;
      cabeca1('NOTA',pag);
     end;
    a:=a-1;
    
  end;
  
	close(arq);
  close(vaga);
  
End.