//registro estrutura de dados

Program segprog;
uses CRT;


type rega=record
	num: integer;
	nome: string[36];
	cpf: string[11];
	data: string[8];	
	cargo: integer;
	n1, n2, n3, n4, n5, som, c_g, c_cl,c_o: integer;
	end;
	
var 
    aux,erro: integer;
	ArqEntrada: text;
	registro:rega;
	ArqSaida: file of rega;
    line, cabecalho: string[60];
    espaco: string[60];


begin
  
  { Monta o cabecalho do arquivo}
  //insert('Num ', cabecalho, 1);
  //insert('Nome                                ', cabecalho, 5);
  //insert('CPF         ', cabecalho, 41);
  //insert('aaaammdd', cabecalho, 52);
  //insert('c', cabecalho, 60);
  //writeln(cabecalho);
  
  {Liga o nome lógico ao físico}
  assign(ArqEntrada,'cadast.TXT');
  assign(ArqSaida,'CADAST.DIR');
  reset(ArqEntrada);
  rewrite(ArqSaida);
  writeln(espaco);

  {Carrega e lê os arquivos}
  while (not EOF(ArqEntrada)) do
  begin
    readln(ArqEntrada,line);
    val(copy(line, 1, 4), aux, erro);
    registro.num:=aux;
    registro.nome:=copy(line,5, 36);
    registro.cpf:= copy(line,41, 11);
    registro.data:= copy(line, 56, 4) + copy(line, 54, 2) + copy(line, 52, 2);
    //insert(registro.data, line,52);
    val(line[60], aux, erro);
    registro.cargo:=aux;
    seek (ArqSaida, registro.num-1);
    write(ArqSaida, registro);       
end;

  {Fecha os arquivos}
  close(ArqSaida);
  close(ArqEntrada);
End.
	
