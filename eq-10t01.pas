  Program arqseq;

  uses modulos, crt;

  type xxx = record
         aa:integer;
         bb:string[3];
         cc:integer;
         end;

  var numa, anoa: string[4];
      nome:string[36];
      cpf:string[11];
      dda,mma, conver, strteste:string[2];
      car:string[4];
      reg:string[60];
      aux,e,a,b,c,d, dds, mms, anos, cari: integer;

      arq:text;
      rec:xxx;
      arq1:file of xxx;

  begin
      clrscr;
      
      {Liga o nome l�gico ao f�sico}
      assign(arq,'cadast.txt'); 
      {Abre um arquivo ja existente para adicionar informa��es}
      append(arq); 

      {Efetua a leitura do numero de inscri�ao e a aloca��o do mesmo no array}
      write('num >');
      readln(numa);
      a:=length(numa);
      for b:=1 to 4-a do
  	    insert('0',numa,b);
        insert(numa, reg,1);

      {Efetua a leitura do nome e a aloca��o do mesmo no array}  
      write('Nome >');
      readln(nome);
      nome:=upcase(nome);
      a:=length(nome);
      for b:=a+1 to 36 do
  	    insert(' ', nome,b);
        insert(nome, reg,5);

      {Efetua a leitura do CPF e a aloca��o do mesmo no array}  
      repeat
         repeat
  	   		write('cpf->');
  	 	   readln(cpf);
             val(cpf,aux,e);
         until(e=0);
  		until(validaCPF(cpf));
  		insert(cpf,reg,41);
  		
      {Efetua a leitura da Data de Nascimento e a aloca��o do mesmo no array}  
  		repeat
  			write('Digite o dia de nascimento (formato: dd) ->');
        readln(dda);
        write('Digite o mes de nascimento (formato: mm) ->');
        readln(mma);
        write('Digite o ano de nascimento (formato: aaaa) ->');
        readln(anoa);
  			val(anoa,anos,e);
  			val(mma,mms,e);
  			val(dda,dds,e);
  		until(valida(anos, mms, dds));
  		a:=length(dda);
      for b:=1 to 2-a do
        insert('0',dda,b);
        insert(dda,reg,52);
      a:=length(mma);
      for b:=1 to 2-a do
        insert('0',mma,b);
  		  insert(mma,reg,54);
  		  insert(anoa,reg,56);

      {Efetua a leitura do cargo e a aloca��o do mesmo no array}  
  		repeat
  			write('Informe o numero do cargo (de 1 � 6) ->');		
  			read(car);
  			val(car,cari,e);
        a:=length(car);
      until (valcar(cari,a));
  		insert(car,reg,60);	

      {Imprime na tela o array}  
      writeln(reg);

      {Registra o array no arquivo}
      writeln(arq,reg);
      
      {Fecha o arquivo}  
      close(arq);
  End.



